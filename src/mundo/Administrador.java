package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import estructuras.AdjacencyList;
import estructuras.Arco;
import estructuras.Queue;
import estructuras.EdmondsChuLiu;
import estructuras.Graph;
import estructuras.Graph.Dijkstra;
import estructuras.IIterador;
import estructuras.Lista;
import estructuras.Nodito;
import estructuras.HashLinear;

public class Administrador {

	public static final String RUTA_TARIFAS = "./data/Tarifas.csv";
	public static final String RUTA_TARIFAS_NACIONALES = "./data/TarifasNacionales.csv";
	public static final String RUTA_VUELOS = "./data/Vuelos.csv";
	public static final String RUTA_VUELOS_NACIONALES = "./data/VuelosNacionales.csv";
	private HashLinear<String, Aerolinea> aerolineas;
	private HashLinear<Integer,VueloInfo> vuelos;
	private Graph<VueloInfo, Conexion> grafoVuelos;
	private Graph<Ciudad, Conexion> grafoCiudades;
	private HashLinear<String, Ciudad> ciudades;
	private int indiceVuelos = 0;
	private int contadorCiudades;
	private HashLinear<String, Graph<Ciudad, Conexion>> tablaAeroGrafo;
	private Graph<VueloInfo, Conexion> grafoCiudadesMST;
	private Graph<VueloInfo, Conexion> grafoMSTAero;



	public Administrador()
	{
		aerolineas = new HashLinear<>(20);
		ciudades = new HashLinear<>();
		vuelos = new HashLinear<Integer,VueloInfo>(1777);	
		tablaAeroGrafo = new HashLinear<>();
	}

	public void cargarYconectar(){
		cargar();
		grafoVuelos = new Graph<VueloInfo,Conexion>(vuelos.size());
		grafoCiudades = new Graph<Ciudad, Conexion>(ciudades.size());
		conectarVuelos();
		conectarCiudades();
		IIterador<String> ite = aerolineas.keys();
		while(ite.hasNext())
		{
			String act = ite.next();
			Aerolinea aero = aerolineas.get(act);
			aero.conectarVuelos();
		}
	}

	public void cargar()
	{

		try 
		{
			@SuppressWarnings("resource")
			BufferedReader l = new BufferedReader(new FileReader(RUTA_TARIFAS_NACIONALES));
			l.readLine();
			String linea = "";
			while ((linea = l.readLine()) != null)
			{

				String [] lineaD = linea.split(",");
				String nombre = lineaD[0];
				Double tarifaMinima = Double.parseDouble(lineaD[1].split(" ")[0]);
				int sillas = Integer.parseInt(lineaD[2]);

				agregarAerolinea(nombre, tarifaMinima, sillas);
			}
			l = new BufferedReader(new FileReader(RUTA_VUELOS_NACIONALES));
			l.readLine();
			String aerolineaActual = "";
			String numeroVueloActual = "";
			String origenActual = "";
			String destinoActual = "";
			String horaSalidaActual = "";
			String horaLlegadaActual = "";
			linea = "";

			while((linea = l.readLine())!=null)
			{
				String [] partes = linea.split(";");
				String aerolinea = partes[0];
				if (aerolinea.isEmpty())
				{
					aerolinea = aerolineaActual;
				}

				String numVuelo = partes[1];
				if (numVuelo.isEmpty())
				{
					numVuelo = numeroVueloActual;
				}

				String origen = partes[2];
				if (origen.isEmpty())
				{
					origen = origenActual;
				}


				String destino = partes[3];
				if (destino.isEmpty())
				{
					destino = destinoActual;
				}

				String horaSalida = partes[4];
				if (horaSalida.isEmpty())
				{
					horaSalida = horaSalidaActual;
				}

				String horaLlegada = partes[5];
				if (horaLlegada.isEmpty())
				{
					horaLlegada = horaLlegadaActual;
				}

				String tipoEquipo = partes[6];
				int numSillas = Integer.parseInt(partes[7]);

				boolean[] dias = new boolean [7];

				for (int i = 0; i < 7; i++)
				{
					dias[i] = !partes[i+8].isEmpty();
				}

				numeroVueloActual = numVuelo;
				aerolineaActual = aerolinea;
				origenActual = origen;
				destinoActual = destino;
				horaSalidaActual = horaSalida;
				horaLlegadaActual = horaLlegada;

				Aerolinea aeroVuelo = aerolineas.get(aerolineaActual);
				VueloInfo aux = agregarVuelo(aeroVuelo.darNombre(), numSillas ,numVuelo, origen, destino, horaSalida, horaLlegada, tipoEquipo, dias);
				agregarCiudadDestino(destino, aux);
				agregarCiudadOrigen(origen, aux);
				aeroVuelo.agregarVuelo(aux);

			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}


	public void conectarVuelos(){
		IIterador<Integer> iter = vuelos.keys();
		while(iter.hasNext()){
			Integer llave = iter.next();
			VueloInfo actual = vuelos.get(llave);
			String origenActual = actual.darCiudadOrigen();
			String destinoActual = actual.darCiudadDestino();
			IIterador<Integer> it = vuelos.keys();
			while (it.hasNext()){
				int cLllave = it.next();
				VueloInfo comparado = vuelos.get(cLllave);
				String origenComp = comparado.darCiudadOrigen();
				String destinoComparado = comparado.darCiudadDestino();
				if (destinoActual.equals(origenComp) && !origenActual.equals(destinoComparado)&&!origenActual.equals(origenComp)){
					boolean[] diasActual = actual.darDias();
					boolean[] diasComparado = comparado.darDias();
					for (int k=0; k<7; k++){
						boolean conectadoEntreSemana = false;
						boolean conectadoFinDeSemana = false;
						if (diasActual[k]){
							for (int h=0;h<7;h++){
								if (diasComparado[h] && (!conectadoEntreSemana||!conectadoFinDeSemana)){
									boolean siguiente = false;
									if (h==k){
										if (comparado.darHoraSalida()<actual.darHoraLlegada()){
											siguiente = true;
										}
									}

									if (h<k||siguiente){
										Double duracion = calcularDuracionT(actual, comparado, k, h, false);
										Double tarifa = calcularTarifa(h, comparado);
										Double tarifaAnterior = calcularTarifa(k, actual);

										if (h<4&&!conectadoEntreSemana){
											conectadoEntreSemana = true;
											agregarConexion(actual, comparado, tarifa, duracion,k, tarifaAnterior);
										}
										else if (h>=4&&!conectadoFinDeSemana){
											conectadoFinDeSemana = true;
											agregarConexion(actual, comparado, tarifa, duracion,k, tarifaAnterior);
										}
									}
									else{
										Double duracion = calcularDuracionT(actual, comparado, k, h, true);
										Double tarifa = calcularTarifa(h, comparado);
										Double tarifaAnterior = calcularTarifa(k, actual);
										if (duracion<=0.0){
											duracion = calcularDuracionT(actual, comparado, k, h, true);
										}
										if (h<4&&!conectadoEntreSemana){
											conectadoEntreSemana = true;
											agregarConexion(actual, comparado, tarifa, duracion,k,tarifaAnterior);
										}
										else if (h>=4&&!conectadoFinDeSemana){
											conectadoFinDeSemana = true;
											agregarConexion(actual, comparado, tarifa, duracion,k,tarifaAnterior);
										}
									}

								}
							}
						}
					}
				}


			}
		}
	}

	public void conectarCiudades(){
		IIterador<Integer> iter = vuelos.keys();
		HashLinear<String, Lista<String>> conexionCiudades = new HashLinear<String, Lista<String>>();
		while(iter.hasNext())
		{
			Integer act = iter.next();
			VueloInfo aux = vuelos.get(act);
			String origen = aux.darCiudadOrigen();
			String desti = aux.darCiudadDestino();

			if(conexionCiudades.get(origen) == null)
			{
				Lista<String> lista = new Lista<String>();
				lista.add(desti);
				conexionCiudades.put(origen, lista);
			}
			else
			{
				Lista<String> lista = conexionCiudades.get(origen);
				IIterador<String> it = lista.darIterador();
				boolean yaExiste = false;
				while (it.hasNext() && !yaExiste)
				{
					String ciudad = it.next();
					if(ciudad.equals(desti))
					{
						yaExiste = true;
					}
				}
				if(!yaExiste)
				{
					lista.add(desti);
				}
			}

		}

		IIterador<String> iterator = conexionCiudades.keys();
		while(iterator.hasNext())
		{
			String actu = iterator.next();
			Ciudad origen = ciudades.get(actu);
			Lista<String> listaDest = conexionCiudades.get(actu);
			IIterador<String> itDestinos = listaDest.darIterador();
			while(itDestinos.hasNext())
			{
				String destino = itDestinos.next();
				Ciudad destino2 = ciudades.get(destino);
				agregarConexionCiudades(origen, destino2);
			}
		}

		System.out.println("Hay " + contadorCiudades + " conexiones entre ciudades");
	}

	public void conectarCiudadesAerolinea (String pAerolinea)
	{
		Aerolinea aero = aerolineas.get(pAerolinea);
		HashLinear<Integer, VueloInfo> hashVuelos = aero.darVuelos();
		IIterador<Integer> iter = hashVuelos.keys();
		HashLinear<String, Lista<String>> coneCiudades = new HashLinear<String, Lista<String>>();
		while(iter.hasNext())
		{
			Integer act = iter.next();
			VueloInfo aux = hashVuelos.get(act);
			String origen = aux.darCiudadOrigen();
			String destino = aux.darCiudadDestino();

			if(coneCiudades.get(origen) == null)
			{
				Lista<String> lista = new Lista<String>();
				lista.add(destino);
				coneCiudades.put(origen, lista);
			}
			else
			{
				Lista<String> lista = coneCiudades.get(origen);
				IIterador<String> it = lista.darIterador();
				boolean yaExiste = false;
				while (it.hasNext() && !yaExiste)
				{
					String ciudad = it.next();
					if(ciudad.equals(destino))
					{
						yaExiste = true;
					}
				}
				if(!yaExiste)
				{
					lista.add(destino);
				}
			}
		}

		Graph<Ciudad, Conexion> grafoAux = new Graph<Ciudad, Conexion>(coneCiudades.size());
		IIterador<String> itrChe = coneCiudades.keys();
		while(itrChe.hasNext())
		{
			String temp = itrChe.next();
			Ciudad origen = ciudades.get(temp);
			Lista<String> listaDes = coneCiudades.get(temp);
			IIterador<String> itD = listaDes.darIterador();


			while(itD.hasNext())
			{
				String destino = itD.next();
				Ciudad destino2 = ciudades.get(destino);
				agregarConexionCiudadGrafo(grafoAux, origen, destino2);
			}
		}
		tablaAeroGrafo.put(pAerolinea, grafoAux);
		System.out.println(grafoAux.componentesConectados());

	}


	public VueloInfo[] darVuelos(){
		VueloInfo[] arregloVuelos = new VueloInfo[vuelos.size()];
		IIterador<Integer> it = vuelos.keys();
		int i=0;
		while(it.hasNext()){
			int k = it.next();
			VueloInfo actual = vuelos.get(k);
			arregloVuelos[i]=actual;
			i++;
		}
		return arregloVuelos;
	}

	public void eliminarCiudad(String pNombreCiudad)
	{
		Ciudad eliminar = ciudades.get(pNombreCiudad);
		Lista<VueloInfo> vuelosEntrada = eliminar.darVuelosEntrada();
		Lista<VueloInfo> vuelosSalida = eliminar.darVuelosSalida();

		IIterador<VueloInfo> iterE = vuelosEntrada.darIterador();
		while (iterE.hasNext()){
			VueloInfo actualE = iterE.next();
			int idE = actualE.darIdentificador();
			vuelos.delete(idE);
			grafoVuelos.removeVertex(actualE);

		}
		IIterador<VueloInfo> iterS = vuelosSalida.darIterador();
		while (iterS.hasNext()){
			VueloInfo actualS = iterS.next();
			int idS = actualS.darIdentificador();
			vuelos.delete(idS);
			grafoVuelos.removeVertex(actualS);
		}
		ciudades.delete(pNombreCiudad);
	}

	public int darNumeroAerolineas(){
		return aerolineas.size();
	}

	public int darNumeroCiudades(){
		return ciudades.size();
	}

	public int darNumeroVuelos(){
		return vuelos.size();
	}

	public void agregarCiudad(String pNombre){
		Ciudad nueva = new Ciudad(pNombre);
		ciudades.put(pNombre, nueva);
	}

	public void agregarAerolinea(String pNombre, Double pTarifa, int pSillas)
	{
		Aerolinea nuevaAero = new Aerolinea(pNombre, pTarifa, pSillas);
		aerolineas.put(pNombre, nuevaAero);
	}

	public void eliminarAerolinea(String pNombre)
	{
		aerolineas.delete(pNombre);
		IIterador<Integer> iter = vuelos.keys();

		while(iter.hasNext())
		{
			Integer key = iter.next();
			VueloInfo act = vuelos.get(key);
			String actualAero = act.darAerolinea().darNombre();
			if (actualAero.equals(pNombre))
			{
				grafoVuelos.removeVertex(act);
			}
		}
	}

	public VueloInfo agregarVuelo(String pAerolinea, int pNumSillas, String numVuelo, String pOrigen, String pDestino, String horaSalida, String horaLlegada, String tipoEquipo, boolean[] pDias)
	{
		Aerolinea aero = aerolineas.get(pAerolinea);
		VueloInfo nuevo = new VueloInfo(Integer.parseInt(numVuelo), pNumSillas, aero, pOrigen, pDestino, horaSalida, horaLlegada, pDias, tipoEquipo);
		indiceVuelos++;
		vuelos.put(indiceVuelos,nuevo);
		return nuevo;
	}


	public void imprimirCiudadesConectadas()
	{
		System.out.println(grafoCiudades.componentesConectados());
	}


	public void imprimirCiudadesConectadasPorAerolinea()
	{
		IIterador<String> it = aerolineas.keys();
		while(it.hasNext())
		{
			String act = it.next();
			System.out.println("Los componentes fuertementeConectados " + act + " son: ");
			conectarCiudadesAerolinea(act);
			System.out.println(" \n");
		}

		IIterador<String> itr = tablaAeroGrafo.keys();
	}





	public void imprimirItiAereo(String ciudadOrigen, String ciudadDestino, int diaPartida)
	{
		IIterador<Aerolinea> itAerolineas = aerolineas.values();
		ArrayList<ItinerarioVuelo> its = new ArrayList<>();
		while(itAerolineas.hasNext())
		{
			Aerolinea actual = itAerolineas.next();
			ItinerarioVuelo it = itinerarioAero(ciudadOrigen, ciudadDestino, diaPartida, Conexion.PESO_COSTO, actual.darNombre());
			if (it!=null){
				its.add(it);
			}
		}
		while (!its.isEmpty()){
			ItinerarioVuelo minIt = its.get(0);
			Double min = minIt.darPesoTotal();
			for (int j=1;j<its.size();j++){
				if (its.get(j).darPesoTotal()<min){
					minIt = its.get(j);
					min = minIt.darPesoTotal();
				}
			}
			System.out.println(minIt);
			System.out.println(minIt.darItinerario());
			System.out.println("Costo total: "+minIt.darPesoTotal());
			System.out.println();
			its.remove(minIt);
		}
	}

	private ItinerarioVuelo itinerarioAero(String ciudadOrigen, String ciudadDestino, int diaPartida, String criterio, String aerolinea)
	{
		IIterador<VueloInfo> itSal = aerolineas.get(aerolinea).darVuelos().values();


		VueloInfo llegada = null;
		VueloInfo salida = null;
		Double pesoMin = 10000.0;

		while(itSal.hasNext())
		{
			VueloInfo actual = itSal.next();

			if (actual.darCiudadOrigen().equals(ciudadOrigen))
			{

				boolean[] diasActual = actual.darDias();
				if (!diasActual[diaPartida])
				{
					continue;
				}
				Dijkstra<VueloInfo, Conexion> dijkstra = new Dijkstra<VueloInfo, Conexion>(aerolineas.get(aerolinea).darGrafo(), actual, criterio);
				IIterador<VueloInfo> itLlegada = aerolineas.get(aerolinea).darVuelos().values();
				while (itLlegada.hasNext())
				{
					VueloInfo actLLega = itLlegada.next();

					if (actLLega.darCiudadDestino().equals(ciudadDestino))
					{
						if (dijkstra.hasPathTo(actLLega))
						{
							Double pesoActual = dijkstra.distTo(actLLega);

							if (pesoActual!=0.0&&pesoActual<pesoMin)
							{
								salida = actual;
								llegada = actLLega;
								pesoMin = pesoActual;
							}
						}
					}
				}
			}
		}
		ItinerarioVuelo iti = null;
		if (salida!=null)
		{
			Dijkstra<VueloInfo, Conexion> diji = new Dijkstra<VueloInfo, Conexion>(grafoVuelos, salida, criterio);
			IIterador<Arco<VueloInfo, Conexion>> iteradorCaminoMasCorto = diji.pathTo(llegada);
			iti = new ItinerarioVuelo(iteradorCaminoMasCorto, criterio);
		}
		return iti;
	}

	public void imprimirIti(String ciudadOrigen, String ciudadDestino, int diaPartida){
		ItinerarioVuelo i = itinerario(ciudadOrigen, ciudadDestino, diaPartida, Conexion.PESO_COSTO);
		StringBuilder sb = i.darItinerario();
		System.out.println(sb);
	}


	private void agregarConexionCiudadGrafo(Graph<Ciudad, Conexion> grafito, Ciudad pOrigen, Ciudad pDestino)
	{
		Conexion conexion = new Conexion(0.0, 0.0, 0, 0.0);
		grafito.addVertex(pOrigen);
		grafito.addVertex(pDestino);
		grafito.addEdge(pOrigen, pDestino, conexion);
		contadorCiudades++;
	}

	private Double calcularDuracionT(VueloInfo actual, VueloInfo otro, int pDiaAct, int pDiaOtro, boolean estaSemana)
	{
		Double resp = 0.0;
		if(estaSemana)
		{

			if (otro.darHoraSalida()<=actual.darHoraSalida()&&pDiaAct!=pDiaOtro){
				resp = (24-actual.darHoraSalida())+((pDiaOtro+1)-(pDiaAct+1)-1)*24+otro.darHoraSalida();
			}
			else if(actual.darHoraSalida()>actual.darHoraLlegada())
			{
				resp = actual.darDuracion() + (otro.darHoraSalida() - actual.darHoraLlegada());
			}
			else
			{
				resp = otro.darHoraSalida()-actual.darHoraSalida();
			}
		}

		else
		{
			resp = (actual.darDuracion()+(24-actual.darHoraLlegada())+(7-(pDiaAct+1))*24.0+(pDiaOtro)*24+otro.darHoraSalida());
		}
		return resp;
	}

	private Double calcularTarifa(int pDia, VueloInfo v){

		Double resp = 0.0;
		if (pDia>=0&&pDia<=3)
		{
			resp = v.costoEntreSemana(); 
		}
		else if (pDia>3&&pDia<7)
		{
			resp = v.costoEntreSemana();
		}
		return resp;
	}

	private void agregarCiudadDestino(String pNombre, VueloInfo pVueloEntrada)
	{
		if(!ciudades.contains(pNombre))
		{
			Ciudad nueva = new Ciudad(pNombre);
			nueva.anhadirVueloEntrada(pVueloEntrada);
			ciudades.put(pNombre, nueva);
		}
		else
		{
			Ciudad actual = ciudades.get(pNombre);
			actual.anhadirVueloEntrada(pVueloEntrada);
		}
	}

	private void agregarCiudadOrigen(String pNombre, VueloInfo pVueloSalida)
	{
		if(!ciudades.contains(pNombre))
		{
			Ciudad nueva = new Ciudad(pNombre);
			nueva.anhadirVueloSalida(pVueloSalida);
			ciudades.put(pNombre, nueva);
		}
		else
		{
			Ciudad actual = ciudades.get(pNombre);
			actual.anhadirVueloSalida(pVueloSalida);
		}
	}

	public void conectarGrafoMST(String ciudad, String aerolinea, int req) throws Exception
	{
		Queue<Object> cola = grafoCiudades.ciudadesConectadasMST();
		HashLinear<Integer, VueloInfo> vuelosHash = new HashLinear<Integer,VueloInfo>();

		boolean cambio = false;

		int cont = 0;
		while(!cambio&&!cola.isEmpty())
		{
			vuelosHash = new HashLinear<Integer, VueloInfo>();
			grafoCiudadesMST = new Graph<>(cola.size());
			while(!cola.isEmpty()&&!cambio)
			{

				Object c = cola.dequeue();
				if(c.toString().equals("cambio"))
				{
					cambio=true;
					break;
				}
				Ciudad ciu = ciudades.get(c.toString());
				IIterador<VueloInfo> it = ciu.darVuelosSalida().darIterador();
				while(it.hasNext())
				{
					VueloInfo v = it.next();
					vuelosHash.put(v.darNumeroVuelo(),v );
				}
			}
			if(vuelosHash.size()<2 && vuelosHash.size()>=0)
				break;

			IIterador<Integer> iter = vuelosHash.keys();

			while(iter.hasNext())
			{
				Integer key = iter.next();
				VueloInfo actual = vuelosHash.get(key);
				String destiAct = actual.darCiudadDestino();
				IIterador<Integer> it = vuelosHash.keys();
				while (it.hasNext())
				{
					Integer key2 = it.next();
					VueloInfo comp = vuelosHash.get(key2);
					String oriComp = comp.darCiudadOrigen();
					if (destiAct.equals(oriComp))
					{
						boolean[] diasActual = actual.darDias();
						boolean[] diasComparado = comp.darDias();
						boolean siguiente = false;

						for (int k=0; k<7&&diasActual[k]; k++)
						{

							for (int h=0;h<7 && diasComparado[k];h++)
							{
								if (h<=k||siguiente)
								{
									Double duracion = calcularDuracionT(actual, comp, k, h, false);
									Double tarifa = calcularTarifa(k, actual);
									agregarMSTConexion(actual, comp, tarifa, duracion, k);
								}
								else
								{
									Double duracion = calcularDuracionT(actual, comp, k, h, true);
									Double tarifa = calcularTarifa(k, actual);
									agregarMSTConexion(actual, comp, tarifa, duracion, k);
								}

							}
						}
					}

				}

			}
			System.out.println(MSTP9(vuelosHash,ciudad,aerolinea,"",req));	
			cont++;
			cambio = false;
		}
	}



	private void agregarMSTConexion(VueloInfo origen, VueloInfo destino, double tarifa, double duracion, int dia)
	{
		Conexion conexion = new Conexion(tarifa, duracion,dia,0.0);
		grafoCiudadesMST.addVertex(origen);
		grafoCiudadesMST.addVertex(destino);
		grafoCiudadesMST.addEdge(origen, destino, conexion);
	}
	private void agregarMSTConexionAero(VueloInfo origen, VueloInfo destino, double tarifa, double duracion, int dia)
	{
		Conexion conexion = new Conexion(tarifa, duracion,dia,0.0);
		grafoMSTAero.addVertex(origen);
		grafoMSTAero.addVertex(destino);
		grafoMSTAero.addEdge(origen, destino, conexion);
	}

	public String MSTP9(HashLinear<Integer, VueloInfo> vuelosEntra, String ciudad, String aerolinea, String dia, int req) throws Exception
	{
		String mensaje = "";
		if(grafoCiudadesMST.vertexCount()!=0)
		{

			Integer key = 0;
			Ciudad ciu = ciudades.get(ciudad);
			IIterador<VueloInfo> itl = ciu.darVuelosSalida().darIterador();
			if(req==1)
			{
				VueloInfo l = itl.next();
				if(l==null)
				{
					System.out.println("El componente no contiene la ciudad indicada");
					throw new Exception("El componente no contiene la ciudad indicada");
				}

			}
			AdjacencyList<VueloInfo, Conexion> adj  = new AdjacencyList<>();
			HashLinear<Nodito<VueloInfo, Conexion>, Lista<Arco<VueloInfo, Conexion>>> ta = new HashLinear<>();
			IIterador<Integer> hola = vuelosEntra.keys();
			int conto = 0;
			while(hola.hasNext()){
				int in = hola.next();
				if(conto == 2)
					key = in;
				VueloInfo vu = vuelosEntra.get(in);
				Nodito<VueloInfo, Conexion> nod = new Nodito<VueloInfo, Conexion>(vu);
				Lista<Arco<VueloInfo, Conexion>> ady = grafoCiudadesMST.darAdyacencias(vu);
				ta.put(nod, ady);
				conto++;
			}


			VueloInfo v = vuelosEntra.get(key);


			Nodito<VueloInfo, Conexion > nodo = new Nodito<VueloInfo, Conexion>(v);

			adj.setAdjacencyList(ta);
			EdmondsChuLiu<VueloInfo, Conexion> ed = new EdmondsChuLiu<>();
			AdjacencyList<VueloInfo, Conexion> resp = ed.getMinBranching(nodo, adj, "duracion");
			IIterador<Nodito<VueloInfo,Conexion>> g = resp.iteradorNodos();
			while(g.hasNext())
			{
				Nodito<VueloInfo,Conexion> t = g.next();
				Lista<Arco<VueloInfo, Conexion>> lista = t.darArcos();
				IIterador<Arco<VueloInfo, Conexion>> listItr = lista.darIterador();
				Arco<VueloInfo, Conexion> arc = null;
				while(listItr.hasNext())
				{
					arc = listItr.next();

				}
				if(arc.darNodoInicio().darID().darIdentificador() == t.darID().darIdentificador()){
					mensaje += ("< " + t.darID().darCiudadOrigen() + " > , < " + t.darID().darCiudadDestino() + "> , <"
							+ t.darID().darAerolinea().darNombre() + " > , < "+ t.darID().darHoraSalida() + "-"+ t.darID().darHoraLlegada()+ " > , < \n");
				}
			}

		}

		return mensaje;
	}

	public void MSTAerolinea(String pCiudad, String pAerolinea)
	{
		Graph<Ciudad, Conexion> grafoChevere = tablaAeroGrafo.get(pAerolinea);
		grafoChevere.componentesConectados();
		Queue<Object> cola = grafoChevere.ciudadesConectadasMST();

		boolean cambio = false;
		int cont =0;
		while(!cambio&&!cola.isEmpty())
		{
			HashLinear<Integer, VueloInfo> vuelosHash = new HashLinear<>();
			grafoMSTAero = new Graph<>(cola.size());
			while (!cola.isEmpty()&&!cambio)
			{
				Object c = cola.dequeue();
				if(c.toString().equals("cambio"))
				{
					cambio=true;
					break;
				}
				Ciudad ciud = ciudades.get(c.toString());
				IIterador<VueloInfo > it = ciud.darVuelosSalida().darIterador();
				while (it.hasNext())
				{
					VueloInfo v = it.next();
					if(v.darAerolinea().darNombre().equals(pAerolinea))
						vuelosHash.put(v.darNumeroVuelo(), v);
				}

			}
			if(vuelosHash.size()>=0&&vuelosHash.size()<2)
				break;

			IIterador<Integer> iter = vuelosHash.keys();
			while(iter.hasNext())
			{
				Integer key = iter.next();
				VueloInfo act = vuelosHash.get(key);
				String destAct = act.darCiudadDestino();
				IIterador<Integer> it = vuelosHash.keys();
				while (it.hasNext())
				{
					Integer key2 = it.next();
					VueloInfo comp = vuelosHash.get(key2);
					String oriComp = comp.darCiudadOrigen();
					if (destAct.equals(oriComp))
					{
						boolean[] diasActual = act.darDias();
						boolean[] diasComparado = comp.darDias();
						boolean siguiente = false;

						for (int k=0; k<7&&diasActual[k]; k++)
						{

							for (int h=0;h<7 && diasComparado[k];h++)
							{

								if (h<=k||siguiente)
								{
									Double duracion = calcularDuracionT(act, comp, k, h, false);
									Double tarifa = calcularTarifa(k, act);
									agregarMSTConexionAero(act, comp, tarifa, duracion, k);
								}
								else
								{
									Double duracion = calcularDuracionT(act, comp, k, h, true);
									Double tarifa = calcularTarifa(k, act);
									agregarMSTConexionAero(act, comp, tarifa, duracion, k);
								}

							}
						}
					}

				}

			}

			System.out.println(requerimiento10(vuelosHash, pCiudad, pAerolinea));
			cont++;
			cambio = false;
		}
		grafoMSTAero = new Graph<VueloInfo, Conexion>(0);

	}

	public String requerimiento10(HashLinear<Integer, VueloInfo> vuelosEntra, String ciudad, String aerolinea )
	{
		String respu = "";
		if(grafoMSTAero.vertexCount()!=0)
		{
			IIterador<VueloInfo> iter =vuelosEntra.values();

			VueloInfo vue = null;
			vue = iter.next();

			HashLinear<Nodito<VueloInfo, Conexion>, Lista<Arco<VueloInfo, Conexion>>> tabla = new HashLinear<>();
			AdjacencyList<VueloInfo, Conexion> adjacency  = new AdjacencyList<>();
			IIterador<Nodito<VueloInfo, Conexion>> it = grafoMSTAero.darNodos();
			while (it.hasNext())
			{
				Nodito<VueloInfo, Conexion> nod = it.next();
				tabla.put(nod, nod.darArcos());
			}
			adjacency.setAdjacencyList(tabla);
			Nodito<VueloInfo, Conexion> nodi = new Nodito<VueloInfo, Conexion>(vue);
			EdmondsChuLiu<VueloInfo, Conexion> edi = new EdmondsChuLiu<>();
			AdjacencyList<VueloInfo, Conexion> adjResp = edi.getMinBranching(nodi, adjacency, "costo");
			IIterador<Nodito<VueloInfo,Conexion>> itrAdj = adjResp.iteradorNodos();
			while(itrAdj.hasNext())
			{
				Nodito<VueloInfo,Conexion> nodito = itrAdj.next();
				Lista<Arco<VueloInfo, Conexion>> lista = nodito.darArcos();
				IIterador<Arco<VueloInfo, Conexion>> listItr = lista.darIterador();
				Arco<VueloInfo, Conexion> arc = null;
				while(listItr.hasNext())
				{
					arc = listItr.next();


				}
				if(nodito.darID().darIdentificador()==arc.darNodoInicio().darID().darIdentificador())
				{
					respu += ("< " + nodito.darID().darCiudadOrigen() + " > , < " + nodito.darID().darCiudadDestino() + "> , <"
							+ nodito.darID().darAerolinea().darNombre() + " > , < "+ nodito.darID().darHoraSalida() + "-"+ nodito.darID().darHoraLlegada()+ " > , < \n");
				}


			}

		}
		return respu;
	}

	public String requerimiento11(HashLinear<Integer, VueloInfo> vuelosEntra, String ciudad,String dia )
	{
		String mensaje = "";
		if(grafoCiudadesMST.vertexCount()!=0)
		{
			IIterador<VueloInfo> ire =vuelosEntra.values();
			VueloInfo vue = null;
			vue = ire.next();
			AdjacencyList<VueloInfo, Conexion> adj  = new AdjacencyList<>();
			HashLinear<Nodito<VueloInfo, Conexion>, Lista<Arco<VueloInfo, Conexion>>> ta = new HashLinear<>();
			IIterador<Nodito<VueloInfo, Conexion>> it = grafoCiudadesMST.darNodos();
			while (it.hasNext())
			{
				Nodito<VueloInfo, Conexion> nod = it.next();
				IIterador<Arco<VueloInfo, Conexion>> listaas = nod.darArcos().darIterador();
				Lista<Arco<VueloInfo, Conexion>> listaFin = new Lista<>();
				while(listaas.hasNext())
				{
					Arco<VueloInfo, Conexion> arc = listaas.next();
					if(darDia(arc.darInformacion().darDia()).equalsIgnoreCase(dia))
						listaFin.add(arc);
				}
				ta.put(nod, listaFin);
			}
			adj.setAdjacencyList(ta);
			Nodito<VueloInfo, Conexion> nodi = new Nodito<VueloInfo, Conexion>(vue);
			EdmondsChuLiu<VueloInfo, Conexion> ed = new EdmondsChuLiu<>();
			AdjacencyList<VueloInfo, Conexion> resp = ed.getMinBranching(nodi, adj, "costo");
			IIterador<Nodito<VueloInfo,Conexion>> g = resp.iteradorNodos();
			while(g.hasNext())
			{
				Nodito<VueloInfo,Conexion> t = g.next();
				Lista<Arco<VueloInfo, Conexion>> lista = t.darArcos();
				IIterador<Arco<VueloInfo, Conexion>> listItr = lista.darIterador();
				Arco<VueloInfo, Conexion> arc = null;
				while(listItr.hasNext())
				{
					arc = listItr.next();


				}
				if(arc.darNodoInicio().darID().darIdentificador() == t.darID().darIdentificador()){
					mensaje += ("< " + t.darID().darCiudadOrigen() + " > , < " + t.darID().darCiudadDestino() + "> , <"
							+ t.darID().darAerolinea().darNombre() + " > , < "+ t.darID().darHoraSalida() + "-"+ t.darID().darHoraLlegada()+ " > , < \n");
					//+ t.darVertice().darDuracion() + " > , < "+ darDia(arc.darInformacion().darDia()) + ">"+ t.darVertice().darNumeroVuelo()+" \n"  );
				}
			}

		}
		return mensaje;
	}

	private String darDia(int pDia){
		String resp = null;
		switch (pDia){
		case 0:
			resp = "Lunes";
			break;
		case 1:
			resp = "Martes";
			break;
		case 2:
			resp = "Miercoles";
			break;
		case 3:
			resp = "Jueves";
			break;
		case 4:
			resp = "Viernes";
			break;
		case 5:
			resp = "Sabado";
			break;
		case 6:
			resp = "Domingo";
			break;
		}
		return resp;
	}

	public void MSTPorDia(String pCiudad, String dia)
	{
		Queue<Object> cola = grafoCiudades.ciudadesConectadasMST();
		HashLinear<Integer, VueloInfo> vuelosHash = new HashLinear<>();

		boolean cambio = false;
		int cont =0;
		while(!cambio&&!cola.isEmpty())
		{
			vuelosHash = new HashLinear<Integer, VueloInfo>();
			grafoCiudadesMST = new Graph<>(cola.size());
			while(!cola.isEmpty()&&!cambio)
			{

				Object c = cola.dequeue();
				if(c.toString().equals("cambio"))
				{
					cambio=true;
					break;
				}
				Ciudad ciu = ciudades.get(c.toString());
				IIterador<VueloInfo> it = ciu.darVuelosSalida().darIterador();
				while(it.hasNext())
				{
					VueloInfo v = it.next();
					vuelosHash.put(v.darNumeroVuelo(),v );
				}
			}
			if(vuelosHash.size()>=0&&vuelosHash.size()<2)
				break;


			IIterador<Integer> iter = vuelosHash.keys();
			while(iter.hasNext())
			{
				Integer llave = iter.next();
				VueloInfo actual = vuelosHash.get(llave);
				String destinoActual = actual.darCiudadDestino();
				IIterador<Integer> it = vuelosHash.keys();
				while (it.hasNext())
				{
					Integer cLllave = it.next();
					VueloInfo comparado = vuelosHash.get(cLllave);
					String origenComp = comparado.darCiudadOrigen();
					if (destinoActual.equals(origenComp))
					{
						boolean[] diasActual = actual.darDias();
						boolean[] diasComparado = comparado.darDias();
						boolean siguiente = false;

						for (int k=0; k<7&&diasActual[k]; k++)
						{

							for (int h=0;h<7 && diasComparado[k];h++)
							{

								if (h<=k||siguiente){
									Double duracion = calcularDuracionT(actual, comparado, k, h, false);
									Double tarifa = calcularTarifa(k, actual);
									agregarMSTConexion(actual, comparado, tarifa, duracion, k);

								}
								else
								{
									Double duracion = calcularDuracionT(actual, comparado, k, h, true);
									Double tarifa = calcularTarifa(k, actual);
									agregarMSTConexion(actual, comparado, tarifa, duracion, k);

								}

							}
						}
					}

				}

			}
			System.out.println(requerimiento11(vuelosHash, pCiudad, dia));
			cambio = false;
		}
	}
	private ItinerarioVuelo itinerario(String ciudadOri, String ciudadDesti, int diaSalida, String criterio)
	{
		Ciudad origen = ciudades.get(ciudadOri);
		Ciudad llegada = ciudades.get(ciudadDesti);

		IIterador<VueloInfo> itr = origen.darVuelosSalida().darIterador();

		VueloInfo salida = null;
		VueloInfo llega = null;
		Double pesoMin = 10000.0;

		while(itr.hasNext())
		{
			VueloInfo act = itr.next();
			boolean[] diasActual = act.darDias();
			if (!diasActual[diaSalida])
				continue;

			Dijkstra<VueloInfo, Conexion> diji = new Dijkstra<VueloInfo, Conexion>(grafoVuelos, act, criterio);

			IIterador<VueloInfo> iterador = llegada.darVuelosEntrada().darIterador();

			while (iterador.hasNext())
			{
				VueloInfo actLlega = iterador.next();
				if (diji.hasPathTo(actLlega))
				{
					Double pesoActual = diji.distTo(actLlega);

					if (pesoActual!=0.0&&pesoActual<pesoMin)
					{
						llega = actLlega;
						salida = act;
						pesoMin = pesoActual;
					}
				}
			}
		}
		Dijkstra<VueloInfo, Conexion> d = new Dijkstra<VueloInfo, Conexion>(grafoVuelos, salida, criterio);

		IIterador<Arco<VueloInfo, Conexion>> itrDiji = d.pathTo(llega);
		ItinerarioVuelo iti = new ItinerarioVuelo(itrDiji, criterio);

		return iti;
	}

	private void agregarConexionCiudades(Ciudad pOrigen, Ciudad pDestino)
	{
		Conexion conexion = new Conexion(0.0,0.0, 0,0.0);
		grafoCiudades.addVertex(pOrigen);
		grafoCiudades.addVertex(pDestino);
		grafoCiudades.addEdge(pOrigen, pDestino, conexion);
		contadorCiudades++;
	}
	private void agregarConexion(VueloInfo origen, VueloInfo destino, Double pTarifa, Double pDuracion, int pDia, Double pCostoAnterior)
	{
		Conexion conexion = new Conexion(pTarifa, pDuracion, pDia, pCostoAnterior);
		grafoVuelos.addVertex(origen);
		grafoVuelos.addVertex(destino);
		grafoVuelos.addEdge(origen, destino, conexion);
	}

}
