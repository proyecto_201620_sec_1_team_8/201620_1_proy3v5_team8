package mundo;

public class VueloInfo implements Comparable<VueloInfo> {
	
	private int numeroVuelo;
	private Aerolinea aerolinea;
	private String origen;
	private String destino;
	private Double horaSalida;
	private Double horaDestino;
	private Double duracion;
	private boolean[] dias;
	private String tipoEsquipo;
	private int numeroSillas;
	private int identificador;
	
	public VueloInfo(int pNumVuelo, int pNumSillas, Aerolinea pAerolinea, String pOrigen, String pDestino, String pHoraSalida, String pHoraLlegada, boolean[] pDias, String pTipo){
		
		origen = pOrigen;
		destino = pDestino;
		aerolinea = pAerolinea;
		numeroVuelo = pNumVuelo;
		definirHorario(pHoraSalida, pHoraLlegada);
		dias = pDias;
		tipoEsquipo = pTipo;
		numeroSillas = pNumSillas;
	}
	
	
	
	public int darNumeroSillas(){
		return numeroSillas;
	}
	
	public int darNumeroVuelo(){
		return numeroVuelo;
	}
	public Aerolinea darAerolinea(){
		return aerolinea;
	}
	
	public String darCiudadOrigen(){
		return origen;
	}
	
	public String darCiudadDestino(){
		return destino;
	}
	public Double darHoraSalida(){
		return horaSalida;
	}
	
	public Double darHoraLlegada(){
		return horaDestino;
	}
	public Double darDuracion(){
		return duracion;
	}
	public void definirHorario(String pHoraSalida, String pHoraLlegada ){
		String[] salidas = pHoraSalida.split(":");
		int hora1 = Integer.parseInt(salidas[0]);
		Double hora2 = ((Integer.parseInt(salidas[1]))/60.0);
		horaSalida = hora1+hora2;
		
		String[] llegada = pHoraLlegada.split(":");
		int hora3 = Integer.parseInt(llegada[0]);
		Double hora4 = ((Integer.parseInt(llegada[1]))/60.0);
		horaDestino = hora3+hora4;
		
		if (horaSalida > horaDestino){
			duracion = (24-horaSalida)+horaDestino;
		}
		else{
			duracion = horaDestino-horaSalida;
		}
	}
	public boolean[] darDias(){
		return dias;
	}
	
	public String darTipoEquipo(){
		return tipoEsquipo;
	}
	
	public int darIdentificador(){
		return identificador;
	}

	public double costoEntreSemana()
	{
		double tarifaMin = aerolinea.darTarifaMinuto();
		Double TariSemMAx = aerolinea.darNumeroSillas()+0.0;
		
		double costo = tarifaMin*(TariSemMAx/numeroSillas)*(duracion*60);
		return costo;
	}
	
	public double CostoEnFinDeSemana()
	{
		double tarifamin = aerolinea.darTarifaMinuto();
		Double tariSemMax = aerolinea.darNumeroSillas()+0.0;
		
		double costo = (tarifamin*(tariSemMax/numeroSillas)*(duracion*60))*(1.3);
		return costo;
	}
	
	@Override
	public int compareTo(VueloInfo o) {
		ComparadorVuelo c = new ComparadorVuelo();
		return c.compare(this, o).intValue();
	}
}