package mundo;

public class AeroCivil {
	
	private Administrador admin;
	public AeroCivil()
	{
		admin  = new Administrador();
	}

	public void cargarArchivo()
	{
		admin.cargarYconectar();
	}

	public void agregarAerolinea (String pNombreAerolinea, Double pTarifaMinuto, int pNumeroSillasMax){
		admin.agregarAerolinea(pNombreAerolinea, pTarifaMinuto, pNumeroSillasMax);
	}

	public void eliminarAerolinea(String pNombreAerolinea){
		admin.eliminarAerolinea(pNombreAerolinea);
	}

	public int darNumeroAerolineas(){
		return admin.darNumeroAerolineas();
	}

	public int darNumeroCiudades(){
		return admin.darNumeroCiudades();
	}
	public int darNumeroVuelos(){
		return admin.darNumeroVuelos();
	}
	public void agregarCiudad(String pNombreCiudad){
		admin.agregarCiudad(pNombreCiudad);
	}
	public void agregarVuelo (String pAerolinea, int pNumSillas, String numVuelo, String pOrigen, String pDestino, String horaSalida, String horaLlegada, String tipoEquipo, boolean[] pDias){
		admin.agregarVuelo(pAerolinea, pNumSillas, numVuelo, pOrigen, pDestino, horaSalida, horaLlegada, tipoEquipo, pDias);
	}
	public void eliminarCiudad (String pNombreCiudad){
		admin.eliminarCiudad(pNombreCiudad);
	}

	public void darCiudadesConectadas()
	{
		admin.imprimirCiudadesConectadas();
	}

	public void darCiudadesConectadasAero()
	{
		admin.imprimirCiudadesConectadasPorAerolinea();
	}

	public void dijistra(String ciudadOrigen, String ciudadDestino, int diaPartida){
		admin.imprimirIti(ciudadOrigen, ciudadDestino, diaPartida);
	}

	public void darMSTCiudades(String ciudad, String aerolinea, int req) throws Exception
	{
		admin.conectarGrafoMST(ciudad, aerolinea, req);
	}
	public void pruebaEdmondsAerolinea(String ciudad, String aerolinea)
	{
		admin.MSTAerolinea(ciudad, aerolinea);
	}
	public void MST11(String ciudad, String dia)
	{
		admin.MSTPorDia(ciudad,dia);
	}
	public void darItinerarioAero(String ciudadOrigen,String ciudadDestino,int diaPartida){
		admin.imprimirItiAereo(ciudadOrigen, ciudadDestino, diaPartida);
	}

	public void darItinerario(String ciudadOrigen, String ciudadDestino, int diaPartida){
		admin.imprimirIti(ciudadOrigen, ciudadDestino, diaPartida);
	}
}
