package mundo;

import estructuras.IComparator;

public class ComparadorVuelo implements IComparator<VueloInfo> 
{

	public Double compare(VueloInfo o1, VueloInfo o2) {
		if (o1.darAerolinea().darNombre().compareTo(o2.darAerolinea().darNombre())>1.0){
			return 1.0;
		}
		return -1.0;
	}

}
