package mundo;

import estructuras.Lista;

public class Ciudad implements Comparable<Ciudad> {
	
	/**
	 * Nombre de la ciudad
	 */
	private String nombre;
	
	/**
	 * Lista de vuelos que entran a la ciudad
	 */
	private Lista<VueloInfo> vuelosEntrada;
	
	/**
	 * Lista de vuelos que sales de la ciudad
	 */
	private Lista<VueloInfo> vuelosSalida;
	
	/**
	 * Constructor de la ciudad, crea una nueva ciudad con el nombre que entra por parametro
	 * @param pNombre Nombre de la ciudad
	 */
	public Ciudad (String pNombre)
	{
		nombre = pNombre;
		vuelosEntrada = new Lista<>();
		vuelosSalida = new Lista<>();
	}
	
	public String darNombre()
	{
		return nombre;
	}
	/**
	 * A�ada el vuelo que entra por parametro a la lista de vuelos que entran a la ciudad
	 * @param pVuelo Vuelo que se desea agregar a la lista
	 */
	public void anhadirVueloEntrada( VueloInfo pVuelo)
	{
		vuelosEntrada.add(pVuelo);
	}
	
	/**
	 * A�ade el vuelo que entra por parametro a la lista de vuelos que salen de la ciudad
	 * @param pVuelo Vuelo que se quiere a�adir
	 */
	public void anhadirVueloSalida (VueloInfo pVuelo)
	{
		vuelosSalida.add(pVuelo);
	}
	
	/**
	 * Retorna una lista con los vuelos que entran a la ciudad
	 * @return Lista de vuelos que entran a la ciudad
	 */
	public Lista<VueloInfo> darVuelosEntrada()
	{
		return vuelosEntrada;
	}
	
	/**
	 * Retorna una lista con los vuelos que salen de la ciudad
	 * @return Lista de vuelos que salen de la ciudad
	 */
	public Lista<VueloInfo> darVuelosSalida()
	{
		return vuelosSalida;
	}

	@Override
	public int compareTo(Ciudad arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString()
	{
		return nombre;
	}
}
