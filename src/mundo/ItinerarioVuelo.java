package mundo;

import java.math.BigDecimal;
import java.math.MathContext;
import estructuras.Arco;
import estructuras.IIterador;

public class ItinerarioVuelo {
	
	private Double peso;
	private StringBuilder string;

	public ItinerarioVuelo(IIterador<Arco<VueloInfo,Conexion>> iterador, String criterio){
		peso=0.0;
		string = new StringBuilder();
		String dia = "";
		if (iterador.hasNext())
		{
			Arco<VueloInfo, Conexion> primer = iterador.next();
			dia = darDia(primer.darInformacion().darDia());
			Double d = primer.darInformacion().darCostoAnterior();
			BigDecimal bd = new BigDecimal(d);
			bd = bd.round(new MathContext(3));
			double rounded1 = bd.doubleValue();
			Double d1 = primer.darInformacion().darCosto();
			BigDecimal bd1 = new BigDecimal(d1);
			bd1 = bd1.round(new MathContext(3));
			double rounded2 = bd1.doubleValue();
			
			string.append(dia+ ":\n");
			string.append(primer.darNodoInicio().darID().darCiudadOrigen() + " - " + primer.darNodoInicio().darID().darCiudadDestino() +
					", vuelo: " + primer.darNodoInicio().darID().darNumeroVuelo() + " Aerolinea: " + primer.darNodoInicio().darID().darAerolinea().darNombre() + " Costo: " + rounded1 + " PESOS \n");
			string.append(primer.darNodoInicio().darID().darCiudadDestino() + " - " + primer.darNodoFin().darID().darCiudadDestino() +
					" , vuelo: " +  primer.darNodoFin().darID().darNumeroVuelo() + " Aerolinea: " + primer.darNodoFin().darID().darAerolinea().darNombre() +" Costo: " + rounded2 + " PESOS \n");
			if (criterio.equals(Conexion.PESO_DURACION))
			{
				peso+=primer.darInformacion().darDuracion();
			}
			else if (criterio.equals(Conexion.PESO_COSTO))
			{
				peso+=primer.darInformacion().darCosto();
				peso+=primer.darInformacion().darCostoAnterior();
			}
		}
		while (iterador.hasNext())
		{
			Arco<VueloInfo,Conexion> actual = iterador.next();
			Double d = actual.darInformacion().darCosto();
			BigDecimal bd = new BigDecimal(d);
			bd = bd.round(new MathContext(3));
			double rounded = bd.doubleValue();
			if (!darDia(actual.darInformacion().darDia()).equals(dia))
			{
				string.append(darDia(actual.darInformacion().darDia()) + ": \n");
			}
			string.append(actual.darNodoFin().darID().darCiudadOrigen() + " - " + actual.darNodoFin().darID().darCiudadDestino()+
				", vuelo: " + actual.darNodoFin().darID().darNumeroVuelo()	+ " Aerolinea: "+ actual.darNodoFin().darID().darNumeroVuelo() +" Costo: "+ rounded + " PESOS \n");
		}
	}
	
	public Double darPesoTotal(){
		return peso;
	}
	
	public StringBuilder darItinerario(){
		return string;
	}
	
	private String darDia(int pDia){
		String resp = null;
		switch (pDia){
		case 0:
			resp = "Lunes";
			break;
		case 1:
			resp = "Martes";
			break;
		case 2:
			resp = "Miercoles";
			break;
		case 3:
			resp = "Jueves";
			break;
		case 4:
			resp = "Viernes";
			break;
		case 5:
			resp = "Sabado";
			break;
		case 6:
			resp = "Domingo";
			break;
		}
		return resp;
	}
}
