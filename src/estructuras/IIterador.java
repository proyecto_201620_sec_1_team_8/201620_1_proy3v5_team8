package estructuras;

public interface IIterador <E>{

	boolean hasNext();
	
	public E next();
	
	public void remove();
	
}
