	package estructuras;

public class Nodito<K extends Comparable<K>,E extends Comparable<E>> {

	private K vertice;

	private Lista<Arco<K,E>> arcos;

	private Nodito<K,E> padre;

	private boolean visitado;

	public Nodito(K pVertice) {
		this.vertice = pVertice;
		this.arcos = new Lista<Arco<K,E>>();
	}

	public K darID() {
		return vertice;
	}

	public Arco<K,E> agregarArco(Nodito<K,E> nodo, Double peso) {
		if (!existeArco(nodo)) {
			Arco<K,E> nuevo = new Arco<K,E>(this, nodo, peso);
			arcos.add(nuevo);
			return nuevo;
		}
		return null;
	}
	
	public Arco<K,E> agregarArco(Nodito<K,E> nodo, E pesos) {
		if (!tieneArco(nodo, pesos)) {
			Arco<K,E> nuevo = new Arco<K,E>(this, nodo, pesos);
			arcos.add(nuevo);
			return nuevo;
		}
		return null;
	}

	public boolean removerArco(Nodito<K,E> nodo) {
		Arco<K,E> actual = null;
		IIterador<Arco<K, E>> it = arcos.darIterador();
		while (it.hasNext()){
			actual = it.next();
			if (actual.darNodoFin().equals(nodo)){
				it.remove();
				return true;
			}
		}
		return false;
	}
	
	public boolean removerArco(Arco<K, E> arco){
		Arco<K,E> actual = null;
		IIterador<Arco<K, E>> it = arcos.darIterador();
		while (it.hasNext()){
			actual = it.next();
			if (actual.equals(arco)){
				it.remove();
				return true;
			}
		}
		return false;
	}

	public boolean tieneArco(Nodito<K,E> comparar, E pesos){
		Arco<K,E> actual = null;
		IIterador<Arco<K, E>> it = arcos.darIterador();
		while (it.hasNext()){
			actual = it.next();
			E infoActual = actual.darInformacion();
			int resp = infoActual.compareTo(pesos);
			boolean menor = resp<=0;
			if (actual.darNodoFin().equals(comparar) && menor){
				return true;
			}
		}
		return false;
	}
	
	public boolean existeArco(Nodito<K,E> comparar){
		Arco<K,E> actual = null;
		IIterador<Arco<K, E>> it = arcos.darIterador();
		while (it.hasNext()){
			actual = it.next();
			if (actual.darNodoFin().equals(comparar)){
				return true;
			}
		}
		return false;
	}
	public int darCantidadArcos() {
		return arcos.size();
	}
	
	public Lista<Arco<K,E>> darArcos() {
		return arcos;
	}

	public Nodito<K,E> darPadre() {
		return padre;
	}

	public boolean fueVisitado() {
		return visitado;
	}

	public void cambiarVisitado(boolean isVisited) {
		this.visitado = isVisited;
	}

	public void cambiarPadre(Nodito<K,E> pPadre) {
		this.padre = pPadre;
	}
}