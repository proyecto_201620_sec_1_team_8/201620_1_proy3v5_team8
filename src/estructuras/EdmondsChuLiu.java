package estructuras;

import mundo.Conexion;

public class EdmondsChuLiu <K extends Comparable<K>,E extends Comparable<E>> {

	private Lista<Nodito<K,E>> cycle;

	public AdjacencyList<K,E> getMinBranching(Nodito<K,E> root, AdjacencyList<K,E> list, String pesoPpal) {
		AdjacencyList<K,E> reverse = list.getReversedList();
		if (reverse.getAdjacent(root) != null) {
			reverse.getAdjacent(root).clear();
		}
		AdjacencyList<K,E> outEdges = new AdjacencyList<K,E>();

		IIterador<Nodito<K, E>> it = reverse.getSourceNodeSet();
		while(it.hasNext())
		{
			Nodito<K,E> nodo = it.next();
			Lista<Arco<K,E>> inEdges = reverse.getAdjacent(nodo);
			if(inEdges.isEmpty())
				continue;
			Arco<K,E> min = inEdges.first();
			IIterador<Arco<K, E>> ed = inEdges.darIterador();
			while(ed.hasNext())
			{
				Arco<K,E> e = ed.next();
				Double pesoActual = ((Conexion)e.darInformacion()).darPeso(pesoPpal);
				Double pesoMin = ((Conexion)min.darInformacion()).darPeso(pesoPpal);
				if(pesoActual<pesoMin)
					min = e;
			}
			outEdges.addEdge(min.darNodoFin(), min.darNodoInicio(), min.darInformacion());
		}

		Lista<Lista<Nodito<K,E>>> cycles = new Lista<Lista<Nodito<K,E>>>();
		cycle = new Lista<>();
		getCycle(root, outEdges);
		cycles.add(cycle);
		IIterador<Nodito<K,E>> iterador = outEdges.getSourceNodeSet();
		while(iterador.hasNext())
		{

			Nodito<K,E> n = iterador.next();
			if(!n.fueVisitado())
			{
				cycle = new Lista<>();
				getCycle(n, outEdges);
				cycles.add(cycle);
			}
		}

		// for each cycle formed, modify the path to merge it into another part of the graph
		AdjacencyList<K,E> outEdgesReverse = outEdges.getReversedList();

		IIterador<Lista<Nodito<K,E>>> itera = cycles.darIterador();
		while(itera.hasNext())
		{
			Lista<Nodito<K,E>> x = itera.next();
			if(x.contains(root))
				continue;
			mergeCycles(x, list, reverse, outEdges, outEdgesReverse,pesoPpal);
		}
		return outEdges;

	}

	private void mergeCycles(Lista<Nodito<K,E>> cycle, AdjacencyList<K,E> list, AdjacencyList<K,E> reverse, AdjacencyList<K,E> outEdges, AdjacencyList<K,E> outEdgesReverse, String pesoPpal) {
		Lista<Arco<K,E>> cycleAllInEdges = new Lista<>();
		Arco<K,E> minInternalEdge = null;
		// find the minimum internal edge weight
		IIterador<Nodito<K,E>> n = cycle.darIterador();
		while(n.hasNext())
		{

			Lista<Arco<K,E>> e = reverse.getAdjacent(n.next());
			IIterador<Arco<K,E>> iterador = e.darIterador();
			while(iterador.hasNext()){
				Arco<K,E> arc = iterador.next();
				if(cycle.contains(arc.darNodoFin())){
					if(minInternalEdge==null||((Conexion)minInternalEdge.darInformacion()).darPeso(pesoPpal)>((Conexion)arc.darInformacion()).darPeso(pesoPpal))
					{
						minInternalEdge = arc;
						continue;
					}
				}
				else{
					cycleAllInEdges.add(arc);
				}
			}


		}
		Arco<K,E> minExternalEdge = null;
		double minModifiedWeight = 0;
		IIterador<Arco<K, E>> it = cycleAllInEdges.darIterador();
		while(it.hasNext())
		{
			Arco<K,E> arc = it.next();
			Lista<Arco<K,E>> listaIn = outEdgesReverse.getAdjacent(arc.darNodoInicio());
			double w = ((Conexion)arc.darInformacion()).darPeso(pesoPpal) - ((Conexion)listaIn.first().darInformacion()).darPeso(pesoPpal);
			if(minExternalEdge==null||minModifiedWeight>w)
			{
				minExternalEdge= arc;
				minModifiedWeight = w;
			}
		}
		Arco<K,E> removing = outEdgesReverse.getAdjacent(minExternalEdge.darNodoInicio()).first();
		outEdgesReverse.getAdjacent(minExternalEdge.darNodoInicio()).clear();
		outEdgesReverse.addEdge(minExternalEdge.darNodoFin(), minExternalEdge.darNodoInicio() ,minExternalEdge.darInformacion());
		Lista<Arco<K,E>> adj = outEdges.getAdjacent(removing.darNodoFin());
		for (IIterador<Arco<K,E>> i = adj.darIterador(); i.hasNext();) {
			if (i.next().darNodoFin() == removing.darNodoInicio()) {
				i.remove();
				break;
			}
		}
		outEdges.addEdge(minExternalEdge.darNodoFin(), minExternalEdge.darNodoInicio(), minExternalEdge.darInformacion());
	}


	private void getCycle(Nodito<K,E> n, AdjacencyList<K,E> outEdges) {
		n.cambiarVisitado(true);
		cycle.add(n);
		if (outEdges.getAdjacent(n) == null) {
			return;
		}
		//        for (Arco<K,E> e : outEdges.getAdjacent(n)) {
		//            if (!e.getTo().isVisited()) {
		//                getCycle(e.getTo(), outEdges);
		//            }
		//        }
		Lista<Arco<K,E>> lista = outEdges.getAdjacent(n);
		IIterador<Arco<K,E>> it = lista.darIterador();
		while(it.hasNext())
		{
			Arco<K,E> e = it.next();
			if(!e.darNodoFin().fueVisitado())
			{
				getCycle(e.darNodoFin(), outEdges);
			}
		}

	}

}
