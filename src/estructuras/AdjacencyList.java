package estructuras;


public class AdjacencyList<K extends Comparable<K>, E extends Comparable<E>> {
	private HashLinear<Nodito<K,E>, Lista<Arco<K,E>>> adjacencies = new HashLinear<>();

	public AdjacencyList() {
	}

	public void setAdjacencyList(HashLinear<Nodito<K,E>, Lista<Arco<K,E>>> pAdjacencies) {
		
		IIterador<Nodito<K,E>> it = pAdjacencies.keys();
		Lista<Arco<K, E>> actual;
		while (it.hasNext()){
			Nodito<K,E> llave = it.next();
			actual = pAdjacencies.get(llave);
			adjacencies.put(llave, actual);
		}
	}

	public void addEdge(Nodito<K,E> source, Nodito<K,E> target, E obj ) {
		Lista<Arco<K,E>> list;
		if (!adjacencies.contains(source)) {
			list = new Lista<Arco<K,E>>();
			adjacencies.put(source, list);
		} else {
			list = adjacencies.get(source);
		}
		list.add(new Arco<K,E>(source, target, obj));
	}


	public Lista<Arco<K, E>> getAdjacent(Nodito<K,E> source) {
		return adjacencies.get(source);
	}


	public void reverseEdge(Arco<K,E> arc){
		IIterador<Arco<K, E>> it = adjacencies.get(arc.darNodoInicio()).darIterador();
		while (it.hasNext()){
			Arco<K, E> actual = it.next();
			if (actual.equals(arc)){
				it.remove();
			}
		}

		addEdge(arc.darNodoFin(), arc.darNodoInicio(), arc.darInformacion());
	}

	public void reverseGraph() {
		setAdjacencies(getReversedList().adjacencies);
	}

	public AdjacencyList<K,E> getReversedList() {
		AdjacencyList<K,E> newlist = new AdjacencyList<K,E>();
		IIterador<Lista<Arco<K, E>>> iter = adjacencies.values();
		while (iter.hasNext()){
			Lista<Arco<K, E>> actual = iter.next();
			IIterador<Arco<K, E>> it = actual.darIterador();
			while (it.hasNext()){
				Arco<K, E> act = it.next();
				newlist.addEdge(act.darNodoFin(), act.darNodoInicio(), act.darInformacion());
			}
		}
		return newlist;
	}

	public 	IIterador<Nodito<K,E>> getSourceNodeSet() {
		return adjacencies.keys();
	}

	/**
	 * 
	 * @return
	 */
	public Lista<Arco<K, E>> getAllEdges() {
		Lista<Arco<K, E>> arcos = new Lista<Arco<K, E>>();
		IIterador<Lista<Arco<K, E>>> listas = adjacencies.values();
		while (listas.hasNext()){
			Lista<Arco<K, E>> actual = listas.next();
			IIterador<Arco<K, E>> itarcos = actual.darIterador();
			while (itarcos.hasNext()){
				Arco<K, E> act = itarcos.next();
				arcos.add(act);
			}
		}
		return arcos;
	}

	private void setAdjacencies(HashLinear<Nodito<K,E>, Lista<Arco<K, E>>> pAdjacencies) {
		this.adjacencies = pAdjacencies;
	}


	public void clear() {
		if (this.adjacencies != null) {
			adjacencies.clear();
		}
	}
	
	public IIterador<Nodito<K, E>> iteradorNodos (){
		return adjacencies.keys();
	}
	
}
